@extends('layouts.app')

@section('content')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-comment"></span> 
                 <b> Historia de usuario  </b>
              
                 <a href="{{ route('pbis.create', ['idsprint' => $idsprint->id]) }}" 
                        class="pull-right btn btn-primary btn-sm">
                        <span 
                        class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Crear nuevo 
                      </a> 
                    
             </h3>
            
        </div> 

        <div class="panel-body">
            
            <ul class="list-group">
                @foreach($pbis as $pbi)
                    <li class="list-group-item"> 
                        <i class="fa fa-play" aria-hidden="true"></i>
                        
                        <a href="/pbis/{{ $pbi->id }}" > {{ $pbi->titulo }}</a> 
                        
                        <a href="/sprints/{{ $pbi->id }}" > {{ $pbi->nombre }}</a>


                        <a href=" /sprints/{{ $pbi->id }} "
                          class="pull-right btn btn-primary btn-sm">  
                          <span 
                          class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>

    

@endsection