@extends('layouts.app')

@section('content')



<div class="row col-md-9 col-lg-9 col-sm-9 pull-left " style="background: white;">
<h1>Actualizar datos de la historia  {{ $pbi->nombre }} </h1>

      <!-- Example row of columns -->
      <div class="row  col-md-12 col-lg-12 col-sm-12" >

                                 
      <form method="post" action="{{ route('pbis.update') }}">
                            {{ csrf_field() }}

                      <input type="hidden" name="pbi_id" value="{{ $pbi->id }}">
                      <input type="hidden" name="sprint_id" value="{{ $pbi->sprint_id }}"> 
                      <input type="hidden" name="_method" value="put">


                      <div class="form-group">
                          <label for="company-name">Nombre<span class="required"></span></label>
                            <input  placeholder="Enter name"  
                                    id="proyecto-name"
                                    required
                                    name="nombre"
                                    spellcheck="false"
                                    class="form-control"
                                    value="{{ $pbi->titulo }}"
                            />
                      </div>

                      <div class="form-group">
                        <label for="company-content">Description</label>
                        <textarea placeholder="Enter description" 
                                  style="resize: vertical" 
                                  id="proyecto-content"
                                  name="descripcion"
                                  rows="4" spellcheck="false"
                                  class="form-control autosize-target text-start">
                                  {{ $pbi->descripcion }}</textarea>
                      </div>

                      <div class="form-group">
                        <label>Seleccione prioridad <span class="glyphicon glyphicon-warning-sign" 
                          aria-hidden="true"></span></label>
                        <select name="priorida" class="selectpicker" data-style="btn-info" style="width:15%;">
                          
                          @foreach( $prioris as $priori)
                            <option value="{{  $priori->id }}">{{ $priori->nombre }}</option>
                          @endforeach
                          
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="company-name">Estimacion de esfuerzo 
                        <input   placeholder="Esfuerzo estimado"  
                                  id="company-name"
                                  required
                                  name="estimacion"
                                  spellcheck="false"
                                  class="form-control"
                                  value="{{ $pbi->estimacion }}"
                                  /> </label>
                    </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary"
                                       value="Editar"/>
                        </div>
       </form>
   

      </div>
</div>


        <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
          
          <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
              <li><a href="/projects/{{ $pbi->id }}"><i class="fa fa-building-o" 
                aria-hidden="true"></i> Ver sprints</a></li>
              <li><a href="/projects"><i class="fa fa-building" 
                aria-hidden="true"></i> Proyectos</a></li>
              
            </ol>
          </div>

        </div>


    @endsection