@extends('layouts.app')

@section('content')



     
     <div class="row col-md-9 col-lg-9 col-sm-9 pull-left " style="background: white;">
    <h1>Nueva historia de usuario  </h1>

      <!-- Example row of columns -->
      <div class="row  col-md-12 col-lg-12 col-sm-12" >

      <form method="post" action="{{ route('pbis.store') }}">
                            {{ csrf_field() }}

      <input type="hidden" name="idSprint" value="{{ /*$pbi->sprint_id*/ $idPbi }}">

                            <div class="form-group">
                                <label for="company-name">titulo<span class="required">*</span></label>
                                <input   placeholder="Titulo de la historia"  
                                          id="company-name"
                                          required
                                          name="name"
                                          spellcheck="false"
                                          class="form-control"
                                           />
                            </div>


                            <div class="form-group">
                                <label for="company-content">Description</label>
                                <textarea placeholder="Descripcion de la historia" 
                                          style="resize: vertical" 
                                          id="company-content"
                                          name="description"
                                          rows="5" spellcheck="false"
                                          class="form-control autosize-target text-left">

                                          
                                          </textarea>
                            </div>

                            <div class="form-group">
                                <label>Seleccione prioridad <span class="glyphicon glyphicon-warning-sign" 
                                  aria-hidden="true"></span></label>
                                <select name="priorida" class="selectpicker" data-style="btn-info" style="width:15%;">
                                  @foreach( $prioris as $priori)
                                    <option value="{{  $priori->id }}">{{ $priori->nombre }}</option>
                                  @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                              <label for="company-name">Estimacion de esfuerzo 
                              <input   placeholder="Esfuerzo estimado"  
                                        id="company-name"
                                        required
                                        name="estimacion"
                                        spellcheck="false"
                                        class="form-control"
                                         /> </label>
                          </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary"
                                       value="Crear"/>
                            </div>
                        </form>
   

      </div>
</div>


<div class="col-sm-3 col-md-3 col-lg-3 pull-right">
          <!--<div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
          </div> -->
          <div class="sidebar-module">
            <h4>Opciones</h4>
            <ol class="list-unstyled">
              <li><a href="/companies"> <i class="fa fa-building-o" aria-hidden="true"></i> Mis sprints</a></li>
              
            </ol>
          </div>

          <!--<div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
              <li><a href="#">March 2014</a></li>
            </ol>
          </div> -->
        </div>


    @endsection