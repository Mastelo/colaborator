@extends('layouts.app')

@section('content')



<div class="row col-md-9 col-lg-9 col-sm-9 pull-left " style="background: white;">
<h1>Actualizar datos del sprint  {{ $project->nombre }} </h1>

      <!-- Example row of columns -->
      <div class="row  col-md-12 col-lg-12 col-sm-12" >

                                 
      <form method="post" action="{{ route('sprints.update') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="sprint_id" value="{{ $project->id }}">
                           {{-- <input type="hidden" name="sprint_id" value="{{ $project->id }}"> --}}
                            <input type="hidden" name="_method" value="put">


                            <div class="form-group">
                                <label for="company-name">Nombre<span class="required"></span></label>
                                <input   placeholder="Enter name"  
                                          id="proyecto-name"
                                          required
                                          name="name"
                                          spellcheck="false"
                                          class="form-control"
                                          value="{{ $project->nombre }}"
                                           />
                            </div>


                            <div class="form-group">
                                <input type="submit" class="btn btn-primary"
                                       value="Submit"/>
                            </div>
                        </form>
   

      </div>
</div>


        <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
          
          <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
              <li><a href="/projects/{{ $project->id }}"><i class="fa fa-building-o" 
                aria-hidden="true"></i> Ver sprints</a></li>
              <li><a href="/projects"><i class="fa fa-building" 
                aria-hidden="true"></i> Proyectos</a></li>
              
            </ol>
          </div>

        </div>


    @endsection