<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//->middleware('auth');


Route::middleware(['auth'])->group(function () {

    Route::resource('companias', 'CompaniesController');

    Route::get('projects/create/{company_id?}', 'ProjectsController@create');
    Route::post('/projects/adduser', 'ProjectsController@adduser')->name('projects.adduser');
    Route::resource('projects', 'ProjectsController');
    
    Route::resource('roles', 'RolesController');
   
    Route::resource('tareas', 'TareasControlador');
    Route::resource('users', 'UsersController');
    Route::resource('comments', 'CommentsController');


    Route::resource('pbis', 'PbisController');
    Route::resource('sprints', 'SprintsControlador');

    Route::get('pbis/edit/{id}', 'PbisController@edit')->name('pbis.edit');
    Route::get('pbis/update', 'PbisController@update')->name('pbis.update');
    Route::get('/pbis/delete/{id}', 'PbisController@destroy')->name('pbis.delete');

    Route::get('sprints', 'SprintsControlador@store');
    Route::post('/sprints/create', 'SprintsControlador@create');
    Route::get('/sprints/delete/{id}', 'SprintsControlador@destroy')->name('sprints.delete');
    Route::get('sprints/edit/{id}', 'SprintsControlador@edit')->name('sprints.edit');
    Route::post('/sprints/update', 'SprintsControlador@update')->name('sprints.update');
    //Route::get('sprints/create', 'ProjectsController@createSprint');
    //Route::get('pbis/create', 'PbisController@store')->name('pbis.store');
});

