<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Pbi;
use App\Sprint;
use App\Prioridad;
use Session;
use Illuminate\Support\Facades\Auth;

class PbisController extends Controller
{
    /*
    public function index()
    {
        
        if( Auth::check() ){


            $companies = Company::where('user_id', Auth::user()->id)->get();

             return view('companies.index', ['companies'=> $companies]);  
        }
        //return view('auth.login');
    }
    /**
      * Display the specified resource.
      *
      * @param  \App\Pbi  $project
      * @return \Illuminate\Http\Response
      */
    
    public function show(Pbi $pbi )
    {
        //dd($pbi) ;
        //$idpbi = Pbi::find($pbi->id);
 
        //$comments = $project->comments;
        $pbis =  \DB::table('tareas')
        ->where('pbi_id', $pbi->id)
        ->get();
        //dd($pbis);
        //revisar
        return view('tareas.show', [  'pbis'=>$pbis ]);
    }


    /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
      public function create(Request $request)
      {
          $idPbi = request()->idsprint;
          //
          //dd($idPbi);
             $pbi = Pbi::where('id', $idPbi)->get()->first();
             $prioris = Prioridad::all();
             //dd($priori);
          return view('pbis.create', compact('pbi','idPbi','prioris'));
      }
  
      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $idsprint = $request->input('idSprint');

            $pbis =  \DB::table('pbis')
            ->where('pbis.sprint_id', $idsprint)
            ->get();

            $this->validate( $request, [
                //'titulo' => 'required',
                'estimacion' => 'numeric',
            ]) ;

          if(Auth::check()){
              $project = Pbi::create([
                  'titulo' => $request->input('name'),
                  'descripcion' => $request->input('description'),
                  'sprint_id' => $request->input('idSprint'), 
                  'prioridad_id' => $request->input('priorida'),  
                  'estimacion' => $request->input('estimacion'),      
              ]);
  
  
              if($project){

                  //return redirect()->route('sprints.show', compact('pbis'/*,'idsprint'*/) )
                  return redirect()->route('sprints.show', ['project'=> $idsprint])
                  ->with('success' , 'Sprint creado');
              }
  
          }
          
              return back()->withInput()->with('errors', 'Error al crear el sprint');
  
      }

    public function edit(/*Pbi $pbi*/ $id)
     {
        $pbi = Pbi::find($id);
        $idpriori = $pbi->prioridad_id;
        $priori = Prioridad::find($idpriori);
        $prioris = Prioridad::all();
        
        return view('pbis.edit', compact ('pbi','priori','prioris'));
     }


     public function update(Request $request)
     {
        $idSprint = $request->input('sprint_id');
        $idPbi = $request->input('pbi_id');
        $modificacion = Pbi::where('id', $idPbi)
                ->update([   
                'titulo'=> $request->input('nombre'),
                'descripcion'=> $request->input('descripcion'),
                'estimacion'=> $request->input('estimacion'),
                'prioridad_id'=> $request->input('priorida')
                ]);
        

       if($modificacion){
            return redirect()->route('sprints.show', ['modificacion'=> $idSprint])
           ->with('success' , 'Se guardaron los datos del pbi');
       }

       return back()->withInput();
     }

     public function destroy( $id)
     {
        $pbi = Pbi::find($id);
         if($pbi->delete()){ 
            Session::flash('success', 'Pbi eliminado') ;
            return redirect()->back(); 
         }         
         return back()->withInput()->with('error' , 'El pbi no puede ser eliminado');
     }
}
